# Chatbot Clustering

## Description
Clustering together tweets to identify types of conversations/questions/problems and identifying the 10 most prominent ones for chatbot developers to focus on using several methods. This was an assignment for the company Freeday/Ciphix. A more detailed description is in the file "Freeday - Case Description V2.pdf".

## Result
There has been found a top ten of subjects, summarised in the file "Clustering.png".

## Files and usage
Below is a list of the files. The case description is the original assignment from the company Freeday. There are two jupyter notebook files, one using TF-IDF and one-hot encoding vectorization, the other word embedding. The latter should be followed first, then the seccond. There is also the data file from Ciphix and two .py-files, one containing functions and the other containing functions and classes for the Word2Vec model used for embedding. For running the code it might be needed to download the NLTK stopwords, as indicated when necessary. 

1. README.md
2. Freeday - Case Description V2.pdf
3. Freeday_data.csv
4. Chatbot tfidf.ipynb
5. Chatbot embedding.ipynb
6. functions.py
7. Word2Vec_fun.py
8. Clustering.pdf

## Contact
Victor Boogers  
VBgetsmail@gmail.com
