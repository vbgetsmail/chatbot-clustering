import re
import numpy as np
import matplotlib.pyplot as plt
import cld3
import operator

from nltk.probability import FreqDist
from wordcloud import WordCloud



# Function to clean certain characters and text
# and put the words in lower case
def cleanText(txt):
    # Remove websites
    txt = re.sub(r'http\S*', '', txt)
    # Remove address
    txt = re.sub(r'@\w*', '', txt)
    # Replace ampersand & by 'and'
    txt = re.sub(r'&amp', 'and', txt)
    # Remove emoticons (Why actually?)
    txt = re.sub(r'\^[A-Z]*', '', txt)
    # Remove sender
    txt = re.sub(r'\-\w*$', '', txt)
    # Replace newlines with spaces for splitting
    txt = re.sub(r'\n', ' ', txt)
    # Replace numbers and various symbols with spaces
    txt = re.sub(r'[,.:;\-\"\'\d\$£#]', ' ', txt)
    # trema's?
    txt = txt.lower()
    return txt



def cleanText_symbols(txt):
    # Replace everything, that's not alphabetic or a white space, with spaces
    txt = re.sub(r'[^a-zA-Z\s]', ' ', txt)
    return txt

    

# Remove certain words/items, contained in a list stopWords, from another list tokens
def remove_stopwords(tokens, stopWords):
    return [w for w in tokens if not w in stopWords]



# Stems the words in a list of strings using stemmer
def list_stemmer(list_words, stemmer):
    return [stemmer.stem(word) for word in list_words]



# Function that sorts the clusters by size, as to grab the biggest clusters.
# Sorts the cluster numbers/labels on frequency, high to low
# Returns a list of cluster numbers sorted and a list of cluster sizes
# per index (unsorted!)
def sort_clusters_by_size(labels, corr=0, scale='linear'):   # corr=-1 for noise label
    N = max(labels)+1
    size_per_cluster = plt.hist(labels, bins=N)[0].tolist()
    plt.yscale(scale)
    size_sorted = sorted(size_per_cluster, reverse=True)
    size_per_cluster2 = size_per_cluster.copy()
    
    cluster_sorted = np.zeros(N, dtype=int)
    for n in range(N):
        cluster_sorted[n] = size_per_cluster2.index(size_sorted[n]) + corr
        # Set to zero, in case of same sizes
        size_per_cluster2[int(cluster_sorted[n]-corr)] = 0
    
    return cluster_sorted, size_per_cluster



# Takes the 'clean text' column of a row and predicts the language.
# Returns the language and the certainty
def get_language(row):
    result = cld3.get_language(row['clean text'])
    return [result.language, result.probability]



# Convert a list of tokens to a sentence by concatenating them
# with spaces in between.
# For compatibility.
def sentencise(token_list):
    sentence = ''
    for token in token_list:
        sentence += ' ' + token
    
    return sentence



# Show three graphs
# First, show the frequency of occurence of the top N_words words.
# Second, show the top N_words words with the highest tfidf-like 
# score in the cluster inthe provided 2D PCA space. The score is  
# similarto tfidf, but uses the overall frequency of the word 
# instead of document frequency.
# Thirdly, show a wordcloud of the cluster.
# The w2v is a mapping from a word to the vector
def vis_txt_clus(Iclus, N_words, data, labels, pca_2d, w2v, plot_dim=(15,12), window=None):
    
    # Get frequencies for words overall and in cluster
    word_list_tot = [item for lst in data['tokens'] for item in lst]
    fdist_tot = FreqDist(word_list_tot)
    word_list = [item for lst in data[labels==Iclus]['tokens'] for item in lst]
    fdist = FreqDist(word_list)
    
    # Plot of word frequencies in descending order
    plt.figure(figsize=(plot_dim[0], plot_dim[1]/1.5))
    plt.yscale('log')
    plt.gca().tick_params(axis='both', which='major', labelsize=20)
    plt.gca().tick_params(axis='both', which='minor', labelsize=15)
    plt.title('Frequency distribution per word in cluster {}'.format(Iclus), fontsize = 20)
    fdist.plot(N_words)
    
    # Calculate and sort by tfidf-like score
    tfidf_dict = {}
    for word, freq in fdist.most_common():
        tfidf_dict[word] = np.log(1+freq) * np.log(1.1+1/fdist_tot[word])
    tfidf_sorted = sorted(tfidf_dict.items(), key=operator.itemgetter(1), reverse=True)
    
    # Scatterplot in PCA space
    n = 0
    x = np.zeros(N_words)
    y = np.zeros(N_words)
    size = np.zeros(N_words)
    plt.figure(figsize=plot_dim)
    if window:
        plt.axis(window)
    ax = plt.gca()
    factor = 40/tfidf_sorted[0][1]
    c = plt.cm.get_cmap('brg', N_words)
    for word, tfidf in tfidf_sorted[:N_words]:
        if word in w2v:
            vec = w2v[word]
            if not hasattr(vec, "__len__"): # len(vec)==1:
                tmp = np.zeros(pca_2d.n_features_)
                tmp[vec] = 1
                vec = tmp
            emb = pca_2d.transform([vec])
            x[n] = emb[0][0]
            y[n] =  emb[0][1]
            size[n] = tfidf
    #         text.append(word)
            plt.text(x[n], y[n], word, fontsize=size[n]*factor, color=c(n), clip_on=True)
            n += 1
    plt.scatter(x, y, s=100*size, clip_on=True)
    plt.xlabel('PC1', fontsize = 20)
    plt.ylabel('PC2', fontsize = 20)
    plt.title('PCA plot of most important words in cluster {}'.format(Iclus), fontsize = 20)

    # Wordcloud
    txtstr = ''.join( [' '+txt for tokens in data[labels==Iclus]['tokens'] for txt in tokens] )
    wc = WordCloud(width=500, height=300, prefer_horizontal=1.0).generate(txtstr)
    plt.figure(figsize=plot_dim)
    plt.imshow(wc, interpolation='bilinear')
    plt.axis('off')
    plt.title('Wordcloud for cluster {}'.format(Iclus), fontsize = 20)
    plt.show()
    
    
    
# Show columns of N_rows rows of clusters in list clus_list
def show_clus_in_df(data, columns, clus_list, N_rows=50):
    for clus in clus_list:
        display(data[columns][data[columns[0]]==clus][:N_rows])